from django.apps import AppConfig


class GitmateLoggerConfig(AppConfig):
    name = 'gitmate_logger'
